ffmpeg \
-protocol_whitelist 'file,udp,rtp,crypto' \
-i offer.sdp \
-speed 4 \
-c:v libx264 \
-x264opts fps=25:keyint=100:min-keyint=100:scenecut=0:noscenecut \
-f mp4 \
out.mp4
