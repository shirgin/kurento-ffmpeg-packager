window.addEventListener('load', function() {
    shaka.polyfill.installAll();

    console.log('shaka player is supported', shaka.Player.isBrowserSupported());

    const player = new shaka.Player(document.querySelector('video'));
    player.addEventListener('error', onErrorEvent);

    if (true) {
        player.load("manifest.mpd").then(() => {
            console.log('manifest loaded');
        }).catch(onError);
    }

    function onError(error) {
        console.error(error);
    }

    function onErrorEvent(event) {
        onError(event.detail);
    }

    document.querySelector('[data-ref=start]').addEventListener('click', function(event) {
        console.log('START');
    });

    document.querySelector('[data-ref=stop]').addEventListener('click', function(event) {
        console.log('STOP');
    });
});
