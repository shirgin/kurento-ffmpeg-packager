const kurento = require('kurento-client');
const config = require('./config');
const {spawn, exec} = require('child_process');
const path = require('path');
const fs = require('fs');

let ffmpeg;
let packager;

console.log('start');

kurento(config.kurento.uri, (err, client) => {
    client.create('MediaPipeline', (err, pipeline) => {
        pipeline.create([
            {type: 'PlayerEndpoint', params: {uri: 'file://' + path.join(process.cwd(), 'video2.webm')}},
            {type: 'RtpEndpoint', params: {}}
        ], (err, elements) => {
            const [player, rtpEndpoint] = elements;

            player.on('EndOfStream', event => {
                pipeline.release();
                player.release();
                rtpEndpoint.release();
                close();
            });

            // rtpEndpoint.setMaxVideoRecvBandwidth(12000);
            // rtpEndpoint.setMaxVideoSendBandwidth(12000);

            player.connect(rtpEndpoint, err => {
                const offer = fs.readFileSync('offer.sdp').toString();
                const time = Date.now().toString();

                rtpEndpoint.processOffer(offer, (err, answer) => {
                    // ffmpeg = spawn(config.ffmpegPath, [
                    //     '-protocol_whitelist', 'file,udp,rtp,crypto',
                    //     '-i', path.join(process.cwd(), 'offer.sdp'),
                    //     '-c', 'copy',
                    //     '-map', '0',
                    //     '-f', 'mp4',
                    //     path.join(process.cwd(), 'out', time + '.mp4')
                    // ]);

                    const dir = path.join('out', time);

                    fs.mkdirSync(dir);

                    exec(`mkfifo ${path.join(dir, 'pipe')}`, function() {
                        const ffmpegArgs = [
                            '-protocol_whitelist', 'file,udp,rtp,crypto',
                            '-i', 'offer.sdp',
                            '-g', '90',
                            '-speed', 4,
                            '-threads', '8',
                            '-c:v', 'libvpx',
                            '-f', 'webm',

                            // 'pipe: > pipe'
                            // `pipe: > ${path.join('out', time, 'pipe')}`
                            path.join('out', time, 'out.webm')
                        ];

                        ffmpeg = spawn(config.ffmpegPath, ffmpegArgs, {cwd: process.cwd()});

                        ffmpeg.stdout.on('data', data => {
                            console.log('FFMPEG OUT:', data.length);
                        });

                        ffmpeg.stderr.on('data', data => {
                            console.log('FFMPEG:', data.toString());
                        });

                        // in=pipe1,stream=audio,init_segment=livehd-audio.webm,segment_template=livehd-audio-\$Number\$.webm \
                        // in=pipe1,stream=video,init_segment=livehd-video.webm,template=livehd-video-\$Number\$.webm \

                        const packagerArgs = [
                            'in=pipe,stream=video,init_segment=live-video.webm,template=\'live-video-$Number$.webm\'',
                            '--mpd_output', 'manifest.mpd',
                            '--profile', 'live',
                            '--dump_stream_info',
                            '--min_buffer_time=10',
                            '--time_shift_buffer_depth=300',
                            '--segment_duration=3',
                            '--io_block_size', '65536'
                        ];

                        console.log(packagerArgs.join(' '));

                        packager = spawn(config.packagerPath, packagerArgs, {cwd: path.join(process.cwd(), 'out', time)});
                        // packager = spawn(config.packagerPath, packagerArgs, {cwd: path.join(process.cwd())});

                        packager.stdout.on('data', data => {
                            console.log('PACKAGER:', data.toString());
                        });

                        packager.stderr.on('data', data => {
                            console.log('PACKAGER:', data.toString());
                        });

                        ffmpeg.on('close', code => {
                            console.log('FFMPEG EXIT', code);
                            packager.kill('SIGINT');
                        });

                        packager.on('close', code => console.log('PACKAGER EXIT', code));

                        player.play(err => {
                            console.log('PLAYING');
                        });
                    });
                });
            });
        });
    });
});

function close() {
    console.log('EXIT');
    if (ffmpeg) {
        ffmpeg.kill('SIGINT');
    }
}
