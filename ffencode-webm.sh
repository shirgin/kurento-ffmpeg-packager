rm -f out.*
ffmpeg \
-r 25 \
-protocol_whitelist 'file,udp,rtp,crypto' \
-i offer.sdp \
-speed 4 \
-c:v libvpx-vp9 \
-keyint_min 100 \
-g 100 \
-r 25 \
-dash 1 \
-f webm \
out.webm

