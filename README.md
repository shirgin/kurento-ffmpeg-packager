# requirements
* ffmpeg with libvpx enabled
* shaka packager
* kurento media server 6.0+

# install
npm install
mkdir out

# configure
in config.json:
* kurento.uri (if not default)
* ffmpegPath (ffmpeg path e.g. "/home/username/Libs/ffmpeg-3.2-64bit-static/ffmpeg" if not added to PATH)
* packagerPath (shaka-packager path e.g. "/home/username/Libs/shaka-packager/src/out/Debug/packager" if not added to PATH)

# run
node server.js
