. ./env.sh
. ./all.sh

ffmpeg \
-protocol_whitelist 'file,udp,rtp,crypto' \
-i offer.sdp \
  -map 0:0 \
  -c:v libvpx-vp9 \
    -keyint_min 60 -g 60 ${VP9_LIVE_PARAMS} \
    -b:v 3000k \
  -f webm_chunk \
    -header "$LOC/glass_360.hdr" \
    -chunk_start_index 1 \
  $LOC/glass_360_%d.chk
