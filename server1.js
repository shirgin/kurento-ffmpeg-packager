const kurento = require('kurento-client');
const config = require('./config');
const {spawn, exec} = require('child_process');
const path = require('path');
const fs = require('fs');

let ffmpeg;
let packager;

console.log('start');

kurento(config.kurento.uri, (err, client) => {
    client.create('MediaPipeline', (err, pipeline) => {
        pipeline.create([
            {type: 'PlayerEndpoint', params: {uri: 'file://' + path.join(process.cwd(), 'video2.webm')}},
            {type: 'RtpEndpoint', params: {}}
        ], (err, elements) => {
            const [player, rtpEndpoint] = elements;

            player.on('EndOfStream', event => {
                pipeline.release();
                player.release();
                rtpEndpoint.release();
                close();
            });

            // rtpEndpoint.setMaxVideoRecvBandwidth(12000);
            // rtpEndpoint.setMaxVideoSendBandwidth(12000);

            player.connect(rtpEndpoint, err => {
                const offer = fs.readFileSync('offer.sdp').toString();
                const time = Date.now().toString();

                rtpEndpoint.processOffer(offer, (err, answer) => {
                    player.play(err => {
                        console.log('PLAYING');
                    });
                });
            });
        });
    });
});

function close() {
    console.log('EXIT');
    if (ffmpeg) {
        ffmpeg.kill('SIGINT');
    }
}
