packager \
in=out.webm,\
stream=video,\
output=vod.webm \
--mpd_output manifest.mpd \
--profile on-demand \
--dump_stream_info \
