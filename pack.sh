packager \
in=out.webm,\
stream=video,\
init_segment=live-video.webm,\
template='live-video-$Number$.webm' \
--mpd_output manifest.mpd \
--profile live \
--dump_stream_info \
--min_buffer_time=10 \
--time_shift_buffer_depth=300 \
--segment_duration=3 \
--io_block_size 65536
